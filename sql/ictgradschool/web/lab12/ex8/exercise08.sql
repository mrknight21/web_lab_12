-- Answers to Exercise 8 here
DELETE FROM dbtest_ex06
WHERE id = 10;
ALTER TABLE dbtest_ex06 DROP member_renting;
DROP TABLE dbtest_ex06;

UPDATE dbtest_EX5
SET username = 'BOSS1'
WHERE username = 'programmer3';

SELECT * FROM dbtest_EX5;

UPDATE dbtest_ex06
SET weekly_charge = 4
WHERE weekly_charge = 6;

UPDATE dbtest_ex06
SET id = 001
WHERE id = 1;

SELECT  * FROM dbtest_ex06;