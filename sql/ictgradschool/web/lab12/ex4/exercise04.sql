-- Answers to Exercise 4 here
DROP TABLE IF EXISTS dbtest_ex04;

CREATE TABLE IF NOT EXISTS dbtest_ex04 (
  id INT NOT NULL,
  title VARCHAR(100),
  content TEXT,
  PRIMARY KEY (id)
);

INSERT INTO dbtest_ex04 (id, title, content) VALUES
  (1,'Steve Kerr Remains Sidelined Indefinitely For Golden State Warriors', 'Golden State Warriors coach Steve Kerr will continue to have medical tests and procedures this week at Stanford. He remains out indefinitely.

Kerr has been sidelined by symptoms, including migraines and nausea, related to complications from the back surgery he had almost two years ago.

Assistant coach Mike Brown will continue to act as head coach of the Warriors for Game 1 against the Utah Jazz on Tuesday, as he did for Games 3 and 4 of the Warriors’ first-round series against the Portland Trail Blazers.

“I think the way we look at it is plan on him [Kerr] not coming back,” Warriors forward Draymond Green told reporters. “That’s the way we’re approaching this thing … Mike Brown is our coach, we have the rest of our staff, and that’s who we’re rolling with.' ),
  (2, 'Ainge amazed by Thomas’ resilience', 'Pick either of the on-court storylines Boston Celtics star Isaiah Thomas has endured — a slow start in the first-round vs. Chicago, getting a tooth knocked out in Game 1 of the East semis vs. Washington — and it would be something for a player to overcome. Yet those trials pale in comparison to Thomas losing his sister and attending her funeral as the playoffs were going on.');

SELECT * FROM dbtest_ex04