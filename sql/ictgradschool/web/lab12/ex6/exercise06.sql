-- Answers to Exercise 6 here
DROP TABLE IF EXISTS dbtest_ex06;

CREATE TABLE IF NOT EXISTS dbtest_ex06 (
  id INT NOT NULL,
  movie_name  VARCHAR(100),
  director  VARCHAR(25),
  weekly_charge INT,
  member_renting VARCHAR(25) references dbtest_videohire(name),
  PRIMARY KEY (id)
);

INSERT INTO dbtest_ex06  (id, movie_name, director, weekly_charge, member_renting) VALUES
  (1, 'The Baby Boss', 'Dweyne Wade', 6, 'Andrew Niccol'),
  (2, 'Mahana', 'Lee Tamahori', 4, 'Russell Crowe'),
  (3, 'Deathgasm', 'Lee Tamahori', 6, 'Lorde'),
  (4, 'Ghost Shark 2: Urban Jaws', 'Jason Lei Howden', 4, 'Ernest Rutherford'),
  (5, 'The Dead Landss', 'Dweyne Wade', 6, 'Andrew Niccol'),
  (6, 'The Hobbit: The Battle of the Five Armies', 'Lebron James', 2, 'Edmund Hillary'),
  (7, 'Hunt for the Wilderpeople', 'Lee Tamahori', 6, 'Sonny Bill Williams'),
  (8, 'Pirates of the Airways', 'Charlie Haskell', 2, 'Bernice Mene'),
  (9, 'Field Punishment No.1', 'Jason Kidd', 2, 'Katherine Mansfield'),
  (10, 'What We Do in the Shadows', 'Jason Lei Howden', 6, 'Bic Runga');



SELECT * FROM dbtest_ex06;