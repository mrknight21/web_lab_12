DROP TABLE IF EXISTS dbtest_EX5;

CREATE TABLE IF NOT EXISTS dbtest_EX5 (
  username VARCHAR(25) NOT NULL,
  fist_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(30),
  PRIMARY KEY (username)
);
-- Answers to Exercise 2 here

INSERT INTO dbtest_EX5 (username, fist_name, last_name, email) VALUES
  ('programmer1', 'Bill', 'Gates','bill@microsoft.com'),
  ('programmer2', 'Mark', 'Zuckerburg', 'mark@facebook.com'),
  ('programmer3', 'Ethan', 'Zuckerman', 'globalvoice@mit.com'),
  ('programmer4', 'Peter', 'Jackson', 'jackson@oggb.com'),
  ('programmer5', 'Pete', 'Penn', 'notpeterpen@story.com'),
  ('programmer6', 'Larry', 'Peterson', 'laPen@france.com');

SELECT * FROM dbtest_usernames;-- Answers to Exercise 5 here
