DROP TABLE IF EXISTS dbtest_usernames;

CREATE TABLE IF NOT EXISTS dbtest_usernames (
  username VARCHAR(25) NOT NULL,
  fist_name VARCHAR(20),
  last_name VARCHAR(20),
  email VARCHAR(30)
);
-- Answers to Exercise 2 here

INSERT INTO dbtest_usernames (username, fist_name, last_name, email) VALUES
('programmer1', 'Bill', 'Gates','bill@microsoft.com'),
('programmer2', 'Mark', 'Zuckerburg', 'mark@facebook.com'),
('programmer3', 'Ethan', 'Zuckerman', 'globalvoice@mit.com'),
('programmer4', 'Peter', 'Jackson', 'jackson@oggb.com'),
('programmer5', 'Pete', 'Penn', 'notpeterpen@story.com'),
('programmer6', 'Larry', 'Peterson', 'laPen@france.com'),
('programmer6', 'Larry', 'Peterson', 'laPen@france.com');

SELECT * FROM dbtest_usernames;


