-- Answers to Exercise 7 here
DROP TABLE IF EXISTS dbtest_ex07;

CREATE TABLE IF NOT EXISTS dbtest_ex07 (
  id INT NOT NULL,
  comment TEXT,
  article_commenting INT references dbtest_ex04(id),
  PRIMARY KEY (id)
);

INSERT INTO dbtest_ex07 VALUES
  (1, 'Hope Kerr will be fine!', 1 ),
  (2, 'Isiah is firing!!', 2),
  (3, 'Kerr and Pop are hilarious when they are together', 1),
  (4, 'I agree!!', 1),
  (5, 'beside Iverson, how can a man less than 1.8 shoot more than 50 point in nba.', 2),
  (6, 'Crazzy!!', 2);

SELECT * FROM dbtest_ex07;